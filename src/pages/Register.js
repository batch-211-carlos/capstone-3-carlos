import { Button, Col, Row, Form, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';



export default function Register() {

    const {user} = useContext(UserContext);

  const navigate = useNavigate();

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [email, setEmail] = useState('');
  const [password1, setPassword1] = useState('');
  const [password2, setPassword2] = useState('');

  const [isActive, setIsActive] = useState('');

    function registerUser(e) {

        e.preventDefault();

        fetch('https://e-commerce2-api-carlos.onrender.com/users/checkEmail',{
        method: "POST",
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email: email
        })
      })
      .then(res=>res.json())
      .then(data=>{
        console.log(data)


        if(data === true){

            Swal.fire({
            title: "Duplicate Email Found!",
            icon: "error",
            text: "Kindly provide another email to complete the registration!"
          })

        }else{

          fetch('https://e-commerce2-api-carlos.onrender.com/users/register',{
            method:"POST",
            headers:{
              'Content-Type':'application/json'
            },
            body: JSON.stringify({
              firstName: firstName,
              lastName: lastName,
              email: email,
              mobileNo: mobileNo,
              password: password1
            })
          })
          .then(res=>res.json())
          .then(data=>{

            if(data===true){

              setFirstName("");
              setLastName("");
              setEmail("");
              setMobileNo("");
              setPassword1("");
              setPassword2("");

              Swal.fire({
                title: "Registration successful!",
                icon: "success",
                text: "Welcome to Anello Store!"
              })

              
              navigate("/login")

            }else{

              Swal.fire({
                title: "Something went wrong!",
                icon: "error",
                text: "Please try again!"

            })



          }
        })
      }
    })


  }

    useEffect(() => {

        if ((firstName !== "" && lastName !== "" && mobileNo.length === 11 && email !== "" && password1 !== "" && password2 !== "") && (password1 === password2)) {
            setIsActive(true)
        } else {
            setIsActive(false)
        }

    }, [firstName, lastName, mobileNo, email, password1, password2])

    return (
        (user.id !== null)
        ?
        <Navigate to="/" />
        :

         <Container className="my-5 py-5 ">
        <div className="d-flex flex-column align-items-center py-5 my-5 card-shadow">
        
            <div className="register">
                <div className="register-container">
                    <div className="register-image">
                       
                       
                       
                    </div>
                    <div className="register-info">
                        <Form onSubmit={(e) => registerUser(e)}>
                            <Row>
                                <h1 className="register-title">Register</h1>
                                <Form.Group as={Col} controlId="firstName">
                                    <Form.Label></Form.Label>
                                    <Form.Control
                                    type="text"
                                    placeholder="First Name"
                                    value = {firstName}
                                    onChange = {e => setFirstName(e.target.value)}
                                    required
                                    />
                                </Form.Group>

                                <Form.Group as={Col} controlId="lastName">
                                    <Form.Label></Form.Label>
                                    <Form.Control
                                    type="text"
                                    placeholder="Last Name"
                                    value = {lastName}
                                    onChange = {e => setLastName(e.target.value)}
                                    required
                                    />
                                </Form.Group>
                            </Row>

                            <Form.Group as={Col}  controlId="mobileNo">
                                <Form.Label></Form.Label>
                                <Form.Control
                                type="text"
                                placeholder="Mobile No."
                                value = {mobileNo}
                                onChange = {e => setMobileNo(e.target.value)}
                                required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="userEmail">
                                <Form.Label></Form.Label>
                                <Form.Control
                                type="email"
                                placeholder="Email"
                                value = {email}
                                onChange = {e => setEmail(e.target.value)}
                                required
                                />
                            </Form.Group>

                            <Form.Group className="mb-3" controlId="password1">
                                <Form.Label></Form.Label>
                                <Form.Control
                                type="password"
                                placeholder="Password"
                                value = {password1}
                                onChange = {e => setPassword1(e.target.value)}
                                required
                                />
                            </Form.Group>

                            

                            { isActive ?
                                <Button className="register-btn"  type="submit">
                                    Register Now
                                </Button>
                                :
                                <Button className="register-btn" type="submit">
                                    Register Now
                                </Button>
                            }

                            <p className="register-terms">Already have an account? <Link to="/login">Login now.</Link></p>

                        </Form>
                    </div>
                </div>
            </div>
            </div>
            </Container>
        
    );
}