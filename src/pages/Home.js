import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home(){

	const data = {
		title: "Welcome to Anello Store",
		content: "Engage in a uniquely Japanese shopping experience, bringing together the best and most innovative fashion and lifestyle brands from the Land of the Rising Sun under one roof is now here in the Philippines!",
		destination: "/login",
		label: "Shop Now!"
	}

	return(
		<>
		
			
			<Highlights/>
			<Banner bannerProp={data}/>
			
		</>
	)
}