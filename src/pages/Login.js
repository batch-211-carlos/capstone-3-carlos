import { Form, Button, Container, Col, Row, Card } from 'react-bootstrap';

import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import { Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';


export default function Login() {


  const [email, setEmail] = useState ('');
  const [password, setPassword] = useState ('');
  const [isActive, setIsActive] = useState ('');

  console.log(email);
  console.log(password);

  const { user, setUser } = useContext(UserContext);

  function authenticate(e) {

      e.preventDefault()


      fetch(`https://e-commerce2-api-carlos.onrender.com/users/login`, {
        method: 'POST',
        headers:{
          'Content-Type':'application/json'
        },
        body: JSON.stringify({
          email: email,
          password: password
        })
      })
      .then(res=>res.json())
      .then(data=>{

       
        console.log(data);

        

        if(typeof data.access !== "undefined"){

         

          localStorage.setItem('token',data.access)


          retrieveUserDetails(data.access)


          Swal.fire({
            title: "Login Successful!",
            icon: "success",
            text: "Welcome to Anello Store!"
          });

        }else{

          Swal.fire({
            title: "Authentication Failed!",
            icon: "error",
            text: "Check your credentials!" 
          });
          
        }
      })

     

      const retrieveUserDetails = (token) =>{

        

        fetch(`https://e-commerce2-api-carlos.onrender.com/users/details`, {
          headers: {
            Authorization: `Bearer ${token}`
          }
        })
        .then(res=>res.json())
        .then(data=>{
          console.log(data);

        

          setUser({
            id: data._id,
            isAdmin: data.isAdmin
          });
        })
      }

     

      setEmail("");
      setPassword("");

      
  }

  useEffect(()=>{

    if(email !== "" && password !== ""){
      setIsActive(true)
    } else {
      setIsActive(false)
    }
 

  }, [email, password])




  return (
    (user.id !== null)
    ?
    <Navigate to="/products"/>
    :
    
    <Container className="my-5 py-5 ">
        <div className="d-flex flex-column align-items-center py-5 my-5 card-shadow">
          
          <Form onSubmit={e => authenticate(e)} className="w-75">
            <Form.Group className="mb-3" controlId="email">
              <Form.Label>Email address</Form.Label>
              <Form.Control 
                type="email" 
                placeholder="Enter email" 
                value={email} 
                onChange={e => setEmail(e.target.value)}
              />
            </Form.Group>
            <Form.Group className="mb-3" controlId="password">
              <Form.Label>Password</Form.Label>
              <Form.Control 
                type="password" 
                placeholder="Password" 
                value={password} 
                onChange={e => setPassword(e.target.value)}
              />
            </Form.Group>
            <div className="d-flex flex-column justify-content-center align-items-center gap-3">
              { isActive ?
                <>
                  <Button type="submit" className="btn btn-primary w-100 p-2">Login</Button>
                  <span>Don't have an account yet? <Link to="/register">Sign up here</Link></span>
                </>
                :
                <>
                  <Button type="submit" className="btn btn-primary w-100 p-2" disabled>Login</Button>
                  <span>Don't have an account yet? <Link to="/register">Sign up here</Link></span>
                </>
              }
            </div>
          </Form>
        </div>
      </Container>



      

    
  );
}
