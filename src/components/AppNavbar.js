import { useContext } from 'react';
import {Navbar, Nav, Container} from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';
import logo from '../logo.png';

export default function AppNavbar(){
 
  const { user } = useContext(UserContext);

  return(
<Navbar bg="blue" expand="lg" variant="dark">
        <Container classname="h-20">

          <Navbar.Brand as={Link} to="/"><img src={logo} width="15% inline"/>Anello Store</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="navy me-auto inline">
              <Nav.Link as={Link} to="/">Home</Nav.Link>
              <Nav.Link as={Link} to="/products">Products</Nav.Link>
              {(user.id !== null) ?
                <Nav.Link as={Link} to="/logout">Logout</Nav.Link>
                :
                <>
                  <Nav.Link as={Link} to="/login">Login</Nav.Link>
                  <Nav.Link as={Link} to="/register">Register</Nav.Link>
                </>
              }
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
  )
}

