import { useContext, useState, useEffect } from "react";
import {Table, Button, Row, Container} from "react-bootstrap";
import {Navigate, Link} from "react-router-dom";
import UserContext from "../UserContext";

import Swal from "sweetalert2";

export default function Dash(){

	// to validate the user role.
	const {user} = useContext(UserContext);

	//Create allCourses State to contain the courses from the database.
	const [allProducts, setAllProducts] = useState([]);

	//"fetchData()" wherein we can invoke if their is a certain change with the course.
	const fetchData = () =>{
		// Get all courses in the database
		fetch(`https://e-commerce2-api-carlos.onrender.com/products/allProducts`,{
			headers:{
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res => res.json())
		.then(data => {
			//console.log(data);

			setAllProducts(data.map(product => {
				return(
					<tr key={product._id}>
						<td>{product._id}</td>
						<td>{product.name}</td>
						<td>{product.description}</td>
						<td>{product.price}</td>
						<td>{product.isActive ? "Active" : "Inactive"}</td>
						<td className="tablecol">
							{
								// We use conditional rendering to set which button should be visible based on the course status (active/inactive)
								(product.isActive)
								?	
								 	<>
									<Button variant="danger" size="sm" className="my-3 mx-3" onClick ={() => archive(product._id, product.name)}>Archive</Button>
									<Button button type="button" class="btn btn-info btn-rounded" as={ Link } to={`/editProduct/${product._id}`}  size="sm" className="my-3 mx-3" >Update</Button>
									
									</>
								:
										<Button variant="secondary" className="my-3 mx-3" size="sm" onClick ={() => unarchive(product._id, product.name)}>Unarchive</Button>
										
										
									
							}
						</td>
					</tr>
				)
			}))

		})
	}



	//Making the course inactive
	const archive = (productId, productName) =>{
		//console.log(productId);
		//console.log(productName);

		fetch(`https://e-commerce2-api-carlos.onrender.com/products/archivedProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: false
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Archive Succesful!",
					icon: "success",
					text: `${productName} is now inactive.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Archive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	//Making the course active
	const unarchive = (productId, productName) =>{
		//console.log(productId);
		//console.log(productName);

		fetch(`https://e-commerce2-api-carlos.onrender.com/products/activateProduct/${productId}`,{
			method: "PUT",
			headers:{
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data =>{
			console.log(data);

			if(data){
				Swal.fire({
					title: "Unarchive Succesful!",
					icon: "success",
					text: `${productName} is now active.`
				})
				fetchData();
			}
			else{
				Swal.fire({
					title: "Unarchive Unsuccessful!",
					icon: "error",
					text: `Something went wrong. Please try again later!`
				})
			}
		})
	}

	// To fetch all courses in the first render of the page.
	useEffect(()=>{
		// invoke fetchData() to get all courses.
		fetchData();
	})
	//***[] dependencies are optional

	return(
	
		(user.isAdmin)
		?
		<>
			<div className="mt-5 mb-3 text-center white-text">
				<h1>Manage Products</h1>
				
				
			</div>
			<div className="right-align mb-3 white-text">
			<Button as={Link} to="/AddProduct" variant="info" size="lg" className="mx-2">Add Product</Button>
			
			
			</div>

			<Table className="nav-shadow white-text">
		     <thead>
		       <tr className="slide-text">
		         <th>Product ID</th>
		         <th>Product Name</th>
		         <th>Description</th>
		         <th>Price</th>
		         <th>Status</th>
		         <th>Category</th>
		         
		       </tr>
		     </thead>
		     <tbody className="slide-text">
		       { allProducts }
		     </tbody>
		   </Table>
		</>

		:
		<Navigate to="/products" />
		
	)
}
