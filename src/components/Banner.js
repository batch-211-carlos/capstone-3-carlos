import {Row, Col, Button} from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({bannerProp}){

	console.log(bannerProp);
	
	const {title, content, destination, label} = bannerProp;

	return (
		
		<div className="banner text-center mt-5">
			<Row>
				<Col>
					<h1><strong>{title}</strong></h1>
					<p>{content}</p>
					<Button as={Link} to={destination}>{label}</Button>
				</Col>
			</Row>
		</div>	
	)
}