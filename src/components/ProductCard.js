import { Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductCard({productProp}){

let { name, description, price, _id } = productProp;

return(
<Card>
<Card.Body>
<Card.Title>{name}</Card.Title>
<Card.Subtitle>Description:</Card.Subtitle>
<Card.Text>{description}</Card.Text>
<Card.Subtitle>Price:</Card.Subtitle>
<Card.Text>PHP {price}</Card.Text>
<Button as={Link} to={`/products/${_id}`}>View Product</Button>
</Card.Body>
</Card>
)
}