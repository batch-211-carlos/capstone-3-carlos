import { useState, useEffect, useContext } from 'react';
import { Container, Card, Form, Button, Row, Col } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import UserContext from '../UserContext';
import Swal from 'sweetalert2';


export default function ProductView(){

    
    const { user } = useContext(UserContext);

    const navigate = useNavigate();

    const { productId } = useParams();

    const [name, setName] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [quantity, setQuantity] = useState(1);
    
    

    const checkout = (productId) =>{

        fetch(`https://e-commerce2-api-carlos.onrender.com/orders/order`,{
            method:'POST',
            headers:{
                'Content-Type':'application/json',
                'Authorization': 'Bearer `${localStorage.getItem(token)`}'
            },
            body: JSON.stringify({
                productId: productId,
                quantity: quantity
                
            }),
        })
        .then(data =>{
            
            if(data===true){
                Swal.fire({
                  title: "Something went wrong!",
                  icon: "error",
                  text: "Check your credentials!"
                });    

            }else{

                Swal.fire({
                  title: "Checkout Successful!",
                  icon: "success",
                  text: "Thank you!"
                });
                navigate("/products");
            }

        })
    }

    useEffect(()=>{
        
        console.log(productId);
        fetch(`https://e-commerce2-api-carlos.onrender.com/products/singleproduct/${productId}`)
        .then(res=>res.json())
        .then(data=>{
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
        
        })

    },[productId])

    return(
        <Container className="mt-5">
          <Row>
             <Col lg={{span:6, offset:3}}>
                <Card>
                    <Card.Body>
                        <Card.Title>{name}</Card.Title>
                        <Card.Subtitle>Description:</Card.Subtitle>
                        <Card.Text>{description}</Card.Text>
                        <Card.Subtitle>Price:</Card.Subtitle>
                        <Card.Text>PHP {price}</Card.Text>
                        <Card.Subtitle>Total Amount:</Card.Subtitle>
                        <Card.Text>PHP {price*quantity}</Card.Text>
                        
                        <Form onSubmit={(e)=>checkout(e)}>
                        <Form.Group className="mb-3 mt-3" controlId="userQuantity">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control
                        type="number"
                        placeholder="Enter quantity"
                        value={quantity}
                        onChange={e=>setQuantity(e.target.value)}
                        required
                        />
                        </Form.Group>
                        </Form>
                        {
                            (user.id!==null)?
                            <Button variant="success" onClick={()=>checkout(productId)}>Checkout</Button>
                            :
                            <Link className="btn btn-danger" to="/login">Sign in to order</Link>
                        }
                        
                    </Card.Body>
                </Card>
             </Col>
          </Row>
        </Container>
    )
}