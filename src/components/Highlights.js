import {Row, Col, Card} from 'react-bootstrap'
import { Carousel } from 'react-bootstrap'



export default function Highlights(){
  return(
    <div id="MyCarousel" class="carousel-fade">

   <Carousel>
      <Carousel.Item interval={1000}>
        <img
          className="d-block w-100"
          src="https://scontent.fmnl17-1.fna.fbcdn.net/v/t39.30808-6/308048075_460040402836570_2591320137430516424_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=e3f864&_nc_ohc=YV1WLmP6argAX8COpzN&_nc_ht=scontent.fmnl17-1.fna&oh=00_AfBz0aU7KcjRGQttjUwUTDdlCbA9xQVBfCtsOqF6WBE8mA&oe=63973BAD"
          alt=" "
        />
        
      </Carousel.Item>

      <Carousel.Item interval={500}>
        <img
          className="d-block w-100"
          src="https://anello.jp/images/mv_22win.jpg"
          alt=" "
        />
        
      </Carousel.Item>

      <Carousel.Item interval={500}>
        <img
          className="d-block w-100"
          src="https://2.bp.blogspot.com/-3kMnDvsPEgo/V9Vg-fLR0-I/AAAAAAAA6wE/TfU0-n56FpgqK7flnWiqtggCbN1j9zvxQCLcB/s1600/header.jpg"
          alt=" "
        />
        
      </Carousel.Item>

       <Carousel.Item interval={500}>
        <img
          className="d-block w-100"
          src="https://anellodirect.com/image/data/banner/1d.jpg"
          alt=" "
        />
        
      </Carousel.Item>
      </Carousel>
    </div>

    )
}

